/*
 *      PROGRAM: displayDrawing.ino
 *      BY: Samantha Ehrle, Anson Kwan
 *      ON: November 25th, 2019
 *      FOR: The 7th and 8th grade STEM students at South Valley Junior High in Gilbert, AZ.
 *      DOES: Displays a drawing using a bitmap
 *      
 *      
 *      This is a slightly more intermediate challenge.
 *      
 *      Create a bitmap to display to the screen.
 *      Two example bitmaps are included: a school bus, and "SVJH"
 *  
 *       Scroll down and look for "SECTION TO EDIT" comments above each editable portion.
 *  
 *  You can change:
 *    The drawing.
 *    The drawing color.
 *    The drawing speed.
 * 
 */

// Libraries to provide us with extra functionality
#include <Adafruit_GFX.h> 
#include <RGBmatrixPanel.h> 

// Constants to define which pins on the Arduino Mega we are using.
// Do not change these!
#define CLK 11
#define OE  9
#define A   A0
#define B   A1
#define C   A2
#define LAT 10
#define n   2

// A constructor: defines which pins of the Arduino will receive what info needed to control the displays.
// Do not change!
RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, false, 32*n);

/**************************************************************************************************
 * 
 *                          SECTION TO EDIT BELOW: DRAWING
 *            
 *                                  
 *                                    EDIT DRAWING:
 *                                  
 *     The array variable named "draw[]" below holds our drawing.
 *     
 *     You can edit the drawing by editing the 0's and 1's in the list of numbers.
 *     
 *     Each string of numbers starts with a 'B' and is followed by 8 numbers which are either 0 or 1.
 *     
 *     'B' means the total list B######## is a byte.
 *     
 *     0 means the LED is off.
 *     1 means the LED is on.
 *     
 *     Make sure every list of numbers is separated by a comma.
 *     Make sure every list of numbers begins with a 'B'.
 *     Make sure every list of numbers has 8 numbers after the B: "B########"
 *     
 *     You will notice there are 16 rows of 4 lists of numbers -- there are also 16 rows on our display.
 *     This means the middle row of the actual displays will be the 8th row of B######## here.
 *     
 *                                   
 *                                   USING THE EXAMPLE DRAWINGS:
 *      
 *      You will notice 2 example draw variables below, one of which are commented out like this text is.
 *      
 *      You can change which one of these will display by making sure it is NOT commented out.
 *      
 *      The other one that you do not want to display must be commented out (the text will look light grey on the Arduino IDE).
 *      
 *      Since both variables have the same name (draw), only one can exist at any time. By commenting out the others, they don't exist.
 *     
 *     
 *                                     EDIT COLORS:
 *                                  
 *     To change the color of your drawing, set "redValue", "greenValue", and "blueValue" to a number between 0 - 7.
 *     
 *     0 means NONE of that color will be mixed in. 7 means A LOT of it will be mixed in.
 *              
 *              For example: redValue = 7, greenValue = 0, blueValue = 0 will display RED.
 *              For example: redValue = 3, greenValue = 0, blueValue = 3 will display PURPLE.
 *              
 *     
 *   
 *                                      EDIT SPEED:
 *                                  
 *     Change the numerical value assigned to "int delayTime".
 *     
 *     Higher numbers = longer delay, slower scroll.
 *     Lower numbers = shorter delay, faster scroll.
 *     It can also be set to 0.
 *      
 ****************************************************************************************************/

// This example draw will display a school bus
static unsigned char PROGMEM const draw[] = {
  B01111111, B11111111, B11111100, B00000000,
  B10100010, B10001010, B00100010, B00000000,
  B10100010, B10001010, B00100010, B00000000,
  B10100010, B10001010, B00100010, B00000000,
  B10111110, B11111011, B11100010, B00000000,
  B10000000, B00000000, B00000011, B11000000,
  B10000000, B00000000, B00000000, B00100000,
  B10000000, B00000000, B00000000, B00100000,
  B10000000, B00000000, B00000000, B00100000,
  B10000000, B00000000, B00000000, B00100000,
  B10000000, B00000000, B00000000, B00100000,
  B01111111, B11111111, B11111111, B11000000,
  B00010010, B00000000, B00010010, B00000000,
  B00100001, B00000000, B00100001, B00000000,
  B00010010, B00000000, B00010010, B00000000,
  B00001100, B00000000, B00001100, B00000000
};

/*
// This example draw will display "SVJH"
// Displaying text is much easier with writeText.ino however!
static unsigned char PROGMEM const draw[] = {
  B11111111, B01000010, B11111111, B01000001,
  B10000000, B01000010, B00010000, B01000001,
  B10000000, B01000010, B00010000, B01000001,
  B10000000, B01000010, B00010000, B01000001,
  B10000000, B01000010, B00010000, B01000001,
  B10000000, B01000010, B00010000, B01000001,
  B10000000, B01000010, B00010000, B01000001,
  B11111111, B01000010, B00010000, B01111111,
  B00000001, B01000010, B00010000, B01000001,
  B00000001, B01000010, B00010000, B01000001,
  B00000001, B01000010, B00010000, B01000001,
  B00000001, B01000010, B00010000, B01000001,
  B00000001, B01000010, B00010000, B01000001,
  B00000001, B01000100, B00010000, B01000001,
  B00000001, B00101000, B00010000, B01000001,
  B11111111, B00010000, B11100000, B01000001
};
*/

// To pick the drawing color, set each of these to a number between 0 - 7
// 0 means none of that color will be there. 7 means a lot of it will.
int redValue = 7,
    greenValue = 4,
    blueValue = 0;

// "delayTime" changes the speed at which the text scrolls.
int delayTime = 2000;

/******************* END SECTION TO EDIT: DRAWING, DRAWING SPEED********************************/
/***********************************************************************************************/


// Set-up function: code that runs only once when the program is first uploaded to the Arduino
void setup() {
  Serial.begin(9600);
  matrix.begin();

  // Clear the screen by turning all the LEDs off
  matrix.fillScreen(0);  

  // A function from our library that creates the drawing using our "draw" variable
  matrix.drawBitmap(0, 0, draw, 32, 16, matrix.Color333(redValue, greenValue, blueValue) );

  // How long the drawing takes to move across the screen
  delay(delayTime);
   
}

// Loop function: this code runs over and over again until manually stopped 
void loop() {
  static uint32_t scrollDelay;
  static int xpos;

  if ( xpos < 65 && ((long) (millis() - scrollDelay) > 0))
  {
    // Clear the screen by turning all the LEDs off
    matrix.fillScreen(0);

    // Creates our drawing, but increases the x position each time (xpos++) so that the
    // drawing appears to "move" slowly to the right
    matrix.drawBitmap(xpos++, 0, draw, 32, 16, matrix.Color333(redValue, greenValue, blueValue) );
    scrollDelay = millis() + 100;
  }

  // Since the total horizontal length of the displays is 64, this means that
  // an x position of 65 corresponds to one column to the right of the displays
  //
  // Once the x position reaches 65, our drawing has gone off the displays
  // Set x position back to 0 so the drawing will start over at the left side
  if (xpos == 65)
  {
    xpos = 0;
    scrollDelay = millis() + 500;
  }
}
 
