/*
 *      PROGRAM: makeColors.ino
 *      BY: Samantha Ehrle, Anson Kwan
 *      ON: November 25th, 2019
 *      FOR: The 7th and 8th grade STEM students at South Valley Junior High in Gilbert, AZ.
 *      DOES: Draws pretty colors
 * .  
 * 
 *    This is just a fun, simple program to mess around with and display pretty colors.
 * 
 *  You can change:
 *    The starting red, green, and blue values
 *    The reset red and green values
 *    The starting x and y values for the top half of the display     
 *    The ending x and y values for the bottom half of the display
 *     
 */

// Libraries to provide us with extra functionality
#include <Adafruit_GFX.h>   // Core graphics library
#include <RGBmatrixPanel.h> // Hardware-specific library

// Constants to define which pins on the Arduino Mega we are using.
// Do not change these!
#define CLK 11 
#define LAT 10
#define OE  9
#define A   A0
#define B   A1
#define C   A2
#define n   2   
RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, false, 32*n);

// The starting red, green, and blue values
// Change these from 0 to 7
uint8_t r = 0,
        g = 0,
        b = 0;

// Change the starting and ending x and y values for both the top and bottom half of the displays
// xStart = 0 to xEnd = 64 is the entire display. This applies to both the top and bottom.
//
// The Y values are a little different:
//    for the top half: yStart = 0 to yEnd = 8 is the entire display.
//    for the bottom half: yStart = 8 to yEnd = 16 is the entire display.
uint8_t xTopStart = 0,
        xTopEnd = 64,
        yTopStart = 0,
        yTopEnd = 8,
        xBottomStart = 0,
        xBottomEnd = 64,
        yBottomStart = 8,
        yBottomEnd = 16;

// When the starting red, green, and blue values reach 8 (which is not a color -- the colors end at 7)
// Then the next line will reset to some color. By default, it is 0.
// Change it to any number between 0 - 7
uint8_t redResetColor = 0,
        greenResetColor = 0;

// Set-up function: code that runs only once when the program is first uploaded to the Arduino
void setup() {
  matrix.begin();

     // fill the screen with 'black'
   matrix.fillScreen(matrix.Color333(0, 0, 0));

  // Draw top half
  for (uint8_t i = xTopStart; i < xTopEnd; i++) {      
    for (uint8_t j = yTopStart; j < yTopEnd; j++) {  
      matrix.drawPixel( i, j, matrix.Color333(r, g, b) );
      r++;
      if (r == 8) {
        r = redResetColor;
        g++;
        if (g == 8) {
          g = greenResetColor;
          b++;
        }
      }
    }
  }

  // Draw bottom half
  for (uint8_t k = xBottomStart; k < xBottomEnd; k++) {      
    for (uint8_t l = yBottomStart; l < yBottomEnd; l++) {  
      matrix.drawPixel(k, l, matrix.Color333(r, g, b));
      r++;
      if (r == 8) {
        r = redResetColor;
        g++;
        if (g == 8) {
          g = greenResetColor;
          b++;
        }
      }
    }
  }
}

// Loop function: this code runs over and over again until manually stopped 
// We don't need to loop here because we write the colors to the display only once
// If you put the void setup() code into loop below, it would flicker because it is
// quickly running over and over and over
void loop() {
 // do nothing
}
