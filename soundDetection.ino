/*
 *      PROGRAM: soundDetection.ino
 *      BY: Samantha Ehrle, Anson Kwan
 *      ON: November 25th, 2019
 *      FOR: The 7th and 8th grade STEM students at South Valley Junior High in Gilbert, AZ.
 *      DOES: Uses a KY-038 sound sensor
 *      
 *  This program can read sound levels and display different messages depending on the noise level.
 *  
 *  You can change:
 *    - The "tooLoud" message
 *    - The "tooQuiet" message
 *    - The sound threshold value that is the difference between "tooLoud" and "tooQuiet"
 *    - The colors and speed of the text that displays
 *  
 */

// Libraries to provide us with extra functionality
#include <Adafruit_GFX.h>   // Core graphics library
#include <RGBmatrixPanel.h> // Hardware-specific library

// Constants to define which pins on the Arduino Mega we are using.
// Do not change these!
#define CLK 11 
#define LAT 10
#define OE  9
#define A   A0
#define B   A1
#define C   A2
#define n   4
#define SOUND_ANALOG_PIN A3     // This is an analog pin on the Arduino Mega that recognizes the specific sound level

// A constructor: defines which pins of the Arduino will receive what info needed to control the displays.
// Do not change!
RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, true, 32*n);

// Global variables that we need to be accessible by our entire program.
// Do not change!
const int maxSize     = 10;
int textX       = matrix.width(),
    textMin     = maxSize * -12,
    currentPass = 0,
    hue         = 0,
    passes      = 0;

// Two messages to display depending on different levels of sound that is detected
// Remember each string (words inside quotes separated by commas) can only be 10 characters each
// You can have as many strings as you want -- just make sure they are separated by commas and each surrounded by quotation marks
// For example: char forecastHigh[][maxSize] = { "Quiet down", "I'm sleepy" };
char tooLoud[][maxSize] = { "Quiet down", "I'm sleepy" };
char tooQuiet[][maxSize] = { "Zzzzz..." };

// This value will define the difference between "tooLoud" and "tooQuiet"
// This corresponds to a voltage: as the sound sensor picks up sound, it changes the voltage
// The Arduino uses the voltage of the sound sensor to figure out how loud it is
double soundThreshold = 50;

// Do you want one color or multi-color?
// "boolean oneColor = true" means single color
// "boolean oneColor = false" means multi-color
boolean oneColor = false;

// For single color, pick the color by changing these 3 values.
// Set each of the three to a number between 0 - 7
// 0 means none of that color will be there. 7 means a lot of it will.
int redValue = 3,
    greenValue = 0,
    blueValue = 3;

// For multi-color, pick how MUCH it changes by changing this "hueIncrease" value.
// A higher number changes colors faster/more drastically, a lower number changes colors slower/less drastically.
int hueIncrease = 35;

// "delayTime" changes the speed at which the text scrolls.
int delayTime = 0;


/******************************** END SECTION TO EDIT: ******************************************/
/***********************************************************************************************/

// Set-up function: code that runs only once when the program is first uploaded to the Arduino
void setup() {
  // Tell the program that the two pins we defined for the sound sensor are inputs to the Arduino
  pinMode( SOUND_ANALOG_PIN, INPUT );

  // Prepare the LED displays (the "matrix") to be printed to
  matrix.begin();
  matrix.setTextWrap(true); // Allow text to run off right edge
  matrix.setTextSize(2); // A text size of 2 takes up the entire display. Larger sizes will get cut off.

  // For printing to the serial monitor
  Serial.begin(9600);
}

// Loop function: this code runs over and over again until manually stopped 
void loop() {

  // Read the sensor value being sent to the Arduino's analog pin and save it to a variable
  int soundReading = analogRead( SOUND_ANALOG_PIN );

  // You can open the Serial Monitor in the Arduino IDE at "Tools > Serial Monitor"
  // To show you the sound readings in real time
  Serial.println( soundReading );

  // Check the current sound level to see if it is higher than the sound threshold we defined above
  // If it is, call the "too Loud" function that prints our message
  // If the sound level is below our sound threshold, call the "too Quiet" function that prints our message
  if ( soundReading > soundThreshold ) printToDisplaySH();
  else printToDisplaySL();

  // We want a time delay before the sensor tries reading values again
  // If it goes too fast, the program may not run well because the sensor can't keep up
  delay(2000);
  
}


// This is the same code as printToDisplaySL except it prints the "TooLoud" message
void printToDisplaySH () {

  // We need to keep track of how big our message is to know how many words to print. Do not change!
  int messageSize = sizeof(tooLoud)/sizeof(tooLoud[0]);
  
  // "currentPass" is a counter variable that keeps track of which words we have printed so far
  // Once it is equal to "messageSize", we know we have printed all the words and need to start over
  while ( currentPass < messageSize ) {

    // Clear the screen by turning every LED off
    matrix.fillScreen(0);

    // Depending on what we set "boolean oneColor" to, set the text color
    // oneColor is true -- set the text to a single color defined by "redValue", "greenValue", and "blueValue"
    if ( oneColor ) matrix.setTextColor(matrix.Color333(redValue, greenValue, blueValue));
    
    // oneColor is false -- set the text to a "hue" that will change as we display the message
    else matrix.setTextColor(matrix.ColorHSV(hue, 255, 255, true));

    // Set the cursor to an X location defined by textX (which will move to the left in order to 'scroll')
    // Y location is set to 1, which corresponds to the second row from the top of the displays
    matrix.setCursor(textX, 1);

    // Print the message!
    matrix.print( tooLoud[currentPass] );

   // These two lines change the color hue IF you have set oneColor to false
   // It changes the color by adding "hueIncrease" to the previous color
   hue += hueIncrease;
   if(hue >= 1536) hue -= 1536;

   // Delay function controls how fast or slow the text moves depending on the value you set for "delayTime"
   delay(delayTime);

   // Decrease textX in order to move the message to the left and 'scroll'
   // Increase currentPass because we have finished printing one of the words and need to move to the next word
    if((--textX) < textMin) {
      textX = matrix.width();
      currentPass++;
    }
    
   matrix.swapBuffers(true);
  }

  // Reset our counter to 0 so we know to print the message from the beginning again
  // This happens once currentPass = messageSize and we break out of the "while" loop above
  currentPass = 0;
}


// This is the same code as printToDisplaySH except it prints the "TooQuiet" message
void printToDisplaySL () {

  // We need to keep track of how big our message is to know how many words to print. Do not change!
  int messageSize = sizeof(tooQuiet)/sizeof(tooQuiet[0]);
  
  // "currentPass" is a counter variable that keeps track of which words we have printed so far
  // Once it is equal to "messageSize", we know we have printed all the words and need to start over
  while ( currentPass < messageSize ) {

    // Clear the screen by turning every LED off
    matrix.fillScreen(0);

    // Depending on what we set "boolean oneColor" to, set the text color
    // oneColor is true -- set the text to a single color defined by "redValue", "greenValue", and "blueValue"
    if ( oneColor ) matrix.setTextColor(matrix.Color333(redValue, greenValue, blueValue));
    
    // oneColor is false -- set the text to a "hue" that will change as we display the message
    else matrix.setTextColor(matrix.ColorHSV(hue, 255, 255, true));

    // Set the cursor to an X location defined by textX (which will move to the left in order to 'scroll')
    // Y location is set to 1, which corresponds to the second row from the top of the displays
    matrix.setCursor(textX, 1);

    // Print the message!
    matrix.print( tooQuiet[currentPass] );

   // These two lines change the color hue IF you have set oneColor to false
   // It changes the color by adding "hueIncrease" to the previous color
   hue += hueIncrease;
   if(hue >= 1536) hue -= 1536;

   // Delay function controls how fast or slow the text moves depending on the value you set for "delayTime"
   delay(delayTime);

   // Decrease textX in order to move the message to the left and 'scroll'
   // Increase currentPass because we have finished printing one of the words and need to move to the next word
    if((--textX) < textMin) {
      textX = matrix.width();
      currentPass++;
    }
    
   matrix.swapBuffers(true);
  }

  // Reset our counter to 0 so we know to print the message from the beginning again
  // This happens once currentPass = messageSize and we break out of the "while" loop above
  currentPass = 0;
}
