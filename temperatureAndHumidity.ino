/*
 *      PROGRAM: tempereatureAndHumidity.ino
 *      BY: Samantha Ehrle, Anson Kwan
 *      ON: November 25th, 2019
 *      FOR: The 7th and 8th grade STEM students at South Valley Junior High in Gilbert, AZ.
 *      DOES: Uses a DHT11 humidity and temperature module
 *      
 *  This program can display the current temperature and humidity to the displays.
 *  
 *  This program can also display a certain "weather forecast" above and below specific values
 *  
 *  
 *  You can change:
 *    The weather forecast above X temperature
 *    The weather forecast below X temperature
 *    What X temperature is
 *    Whether or not the display shows temperature/humidity or your inputted forecasts
 * 
 */

// Libraries to provide us with extra functionality
#include <Adafruit_GFX.h>   // Core graphics library
#include <RGBmatrixPanel.h> // Hardware-specific library
#include <stdlib.h>         

// The DHT11 Temperature/Humidity module library.
// This library can be found in Arduino library search or here: https://github.com/adafruit/DHT-sensor-library
// This library needs the "AdaFruit Unified Sensor Library" installed: https://github.com/adafruit/Adafruit_Sensor
#include <DHT.h>
#include <DHT_U.h>

// Constants to define which pins on the Arduino Mega we are using.
// Do not change these!
#define CLK 11 
#define LAT 10
#define OE  9
#define A   A0
#define B   A1
#define C   A2
#define n   4
#define LED_PIN 52      // This is a digital pin that controls the lighting of an LED
#define TEMP_PIN A7     // This is an analog pin on the Arduino Mega that receives the temp/humidity info from the sensor
#define TEMP_TYPE DHT11 // This defines which version of the DHT module we are using: DHT11

// This creates a DHT temp/humidity module "object"
DHT dht( TEMP_PIN, TEMP_TYPE ); 

// A constructor: defines which pins of the Arduino will receive what info needed to control the displays.
// Do not change!
RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, true, 32*n);

// Global variables that we need to be accessible by our entire program.
// Do not change!
const int maxSize     = 10;
int textX       = matrix.width(),
    textMin     = maxSize * -12,
    currentPass = 0,
    hue         = 0,
    passes      = 0;
double humidity, temperature;

// Function "prototypes" which tell the compiler these are going to exist later
// Do not change!
void printToDisplayFH ();
void printToDisplayFL ();
void printToDisplayTH ();

// Two forecast messages for higher temps and lower temps
// Either one of these will display depending on what the sensor senses
//
// The boolean variable "showForecast" needs to be true for these to display.
//
// Remember each string (words inside quotes separated by commas) can only be 10 characters each
// You can have as many strings as you want -- just make sure they are separated by commas and each surrounded by quotation marks
// For example: char forecastHigh[][maxSize] = { "Forecast:", "Sweaty pits" };
char forecastHigh[][maxSize] = { "Forecast:", "Sweaty pits" };
char forecastLow[][maxSize] = { "Forecast:", "Still hot", "It's AZ" };

// The message that will display the current temperature in fahrenheit and humidity %
char currentTempAndHumidity[4][maxSize] = { "Temp: ", "0", "Humidity: ", "0"};

// Do you want to show one of the two forecasts you inputted above?
// Or do you want to show the temperature and humidity readings?
// "boolean showForecast = true" will show one of your forecasts depending on the current temperature and temperatureThreshold
// "boolean showForecast = false" will show the current temperature and humidity readings, but not display your forecasts
boolean showForecast = false;

// This value will define the difference between "forecastHigh" and "forecastLow"
// A temperature above this number means "forecastHigh" will display
// A temperature below this number means "forecastLow" will display
// Use fahrenheit
double temperatureThreshold = 75;

// Do you want one color or multi-color?
// "boolean oneColor = true" means single color
// "boolean oneColor = false" means multi-color
boolean oneColor = false;

// For single color, pick the color by changing these 3 values.
// Set each of the three to a number between 0 - 7
// 0 means none of that color will be there. 7 means a lot of it will.
int redValue = 3,
    greenValue = 0,
    blueValue = 3;

// For multi-color, pick how MUCH it changes by changing this "hueIncrease" value.
// A higher number changes colors faster/more drastically, a lower number changes colors slower/less drastically.
int hueIncrease = 35;

// "delayTime" changes the speed at which the text scrolls.
// Bigger number = longer delay = slower scroll
// Smaller number = shorter delay = faster scroll
int delayTime = 0;

// These variables exist so that we can convert the numbers our sensor reads (doubles) into words (Strings)
String temp, humid;


/******************************** END SECTION TO EDIT: ******************************************/
/***********************************************************************************************/

// Set-up function: code that runs only once when the program is first uploaded to the Arduino
void setup() {
  // Tell the program that the TEMP_PIN we defined as pin #A7 above is used as input
  pinMode(TEMP_PIN, INPUT);

  // Prepare the LED displays (the "matrix") to be printed to
  matrix.begin();
  matrix.setTextWrap(true); // Allow text to run off right edge
  matrix.setTextSize(2); // A text size of 2 takes up the entire display. Larger sizes will get cut off.

  // Initialize the DHT sensor
  dht.begin();

  // For printing to the serial monitor
  Serial.begin(9600);
}

// Loop function: this code runs over and over again until manually stopped 
void loop() {
  
  // We need to know which function to call: displayForecasts or displayTemp?
  // We pick by checking the boolean variable "showForecasts"
  
  // If it is true, call the displayForecasts function
  if ( showForecast ) displayForecasts();
  
  // If showForecast is false, call the displayTemp function
  else displayTemp();

  // We want a time delay before the sensor tries reading values again
  delay(2000);
}


// Displays one of the two forecasts you created depending upon the current temperature
void displayForecasts() {
  
 // Reads and stores the current temperature being read by the sensor.
 // Converts a double into an int using the (int) 
 temperature = (int) (dht.readTemperature() * (9.0/5.0) + 32);

 // If the current temperature is above the temperatureThreshold, display forecastHigh
 if ( temperature > temperatureThreshold ) printToDisplayFH();

 // If the current temperature is below the temperatureThreshold, display forecastLow
 else printToDisplayFL();
  
}


// Displays the current temperature and humidity to the displays
void displayTemp () {
  
  // These variables read and store the humidity and temperature values the sensor picks up
  // The temperature value is converted from Celsius to Fahrenheit
  int humidity = (int) dht.readHumidity(); 
  int temperature = (int) (dht.readTemperature() * (9.0/5.0) + 32);

  // Conver the integers (ints) above into Strings
  humid = String(humidity);
  temp = String(temperature);
  
  // Put the strings into a char array for printing
  Serial.println(humid.length());
  char h[2],t[temp.length()];

  humid.toCharArray(h,humid.length()+1);
  temp.toCharArray(t,temp.length()+1);
  
  // Open the serial monitor in the Arduino IDE at tools > "Serial Monitor" to see the temp/humidity in real time
  Serial.println( "Humidity: " + humid );
  Serial.println( "Temp: " + temp );

  clearRow ( 1 , sizeof(t));
  clearRow ( 3 , sizeof(h));
  populateRow ( 1 , t );
  populateRow ( 3 , h );

  printToDisplayTH ();
  
}

// A helper function that clears the char array of the Strings (rows)
void clearRow (int row, int len) {
  for(int i = 0; i < len; i++){
    currentTempAndHumidity[row][i] = '\0';
  }
}

// A helper function that re-fills the rows after we cleared them with clearRow
void populateRow (int row, char num[]) {
  for(int i = 0; i < sizeof(num); i++){
    currentTempAndHumidity[row][i] = num[i];
  }
}

// This prints the forecastHigh message to the display
void printToDisplayFH () {

  // We need to keep track of how big our message is to know how many words to print. Do not change!
  int messageSize = sizeof(forecastHigh)/sizeof(forecastHigh[0]);
  
  // "currentPass" is a counter variable that keeps track of which words we have printed so far
  // Once it is equal to "messageSize", we know we have printed all the words and need to start over
  while ( currentPass < messageSize ) {

    // Clear the screen by turning every LED off
    matrix.fillScreen(0);

    // Depending on what we set "boolean oneColor" to, set the text color
    // oneColor is true -- set the text to a single color defined by "redValue", "greenValue", and "blueValue"
    if ( oneColor) matrix.setTextColor(matrix.Color333(redValue, greenValue, blueValue));
    
    // oneColor is false -- set the text to a "hue" that will change as we display the message
    else matrix.setTextColor(matrix.ColorHSV(hue, 255, 255, true));

    // Set the cursor to an X location defined by textX (which will move to the left in order to 'scroll')
    // Y location is set to 1, which corresponds to the second row from the top of the displays
    matrix.setCursor(textX, 1);

    // Print the message!
    matrix.print( forecastHigh[currentPass] );

   // These two lines change the color hue IF you have set oneColor to false
   // It changes the color by adding "hueIncrease" to the previous color
   hue += hueIncrease;
   if(hue >= 1536) hue -= 1536;

   // Delay function controls how fast or slow the text moves depending on the value you set for "delayTime"
   delay(delayTime);

   // Decrease textX in order to move the message to the left and 'scroll'
   // Increase currentPass because we have finished printing one of the words and need to move to the next word
    if((--textX) < textMin) {
      textX = matrix.width();
      currentPass++;
    }
    
   matrix.swapBuffers(true);
  }

  // Reset our counter to 0 so we know to print the message from the beginning again
  // This happens once currentPass = messageSize and we break out of the "while" loop above
  currentPass = 0;
}

// This prints the forecastLow message to the display
void printToDisplayFL () {

  // We need to keep track of how big our message is to know how many words to print. Do not change!
  int messageSize = sizeof(forecastLow)/sizeof(forecastLow[0]);
  
  // "currentPass" is a counter variable that keeps track of which words we have printed so far
  // Once it is equal to "messageSize", we know we have printed all the words and need to start over
  while ( currentPass < messageSize ) {

    // Clear the screen by turning every LED off
    matrix.fillScreen(0);

    // Depending on what we set "boolean oneColor" to, set the text color
    // oneColor is true -- set the text to a single color defined by "redValue", "greenValue", and "blueValue"
    if ( oneColor) matrix.setTextColor(matrix.Color333(redValue, greenValue, blueValue));
    
    // oneColor is false -- set the text to a "hue" that will change as we display the message
    else matrix.setTextColor(matrix.ColorHSV(hue, 255, 255, true));

    // Set the cursor to an X location defined by textX (which will move to the left in order to 'scroll')
    // Y location is set to 1, which corresponds to the second row from the top of the displays
    matrix.setCursor(textX, 1);

    // Print the message!
    matrix.print( forecastLow[currentPass] );

   // These two lines change the color hue IF you have set oneColor to false
   // It changes the color by adding "hueIncrease" to the previous color
   hue += hueIncrease;
   if(hue >= 1536) hue -= 1536;

   // Delay function controls how fast or slow the text moves depending on the value you set for "delayTime"
   delay(delayTime);

   // Decrease textX in order to move the message to the left and 'scroll'
   // Increase currentPass because we have finished printing one of the words and need to move to the next word
    if((--textX) < textMin) {
      textX = matrix.width();
      currentPass++;
    }
    
   matrix.swapBuffers(true);
  }

  // Reset our counter to 0 so we know to print the message from the beginning again
  // This happens once currentPass = messageSize and we break out of the "while" loop above
  currentPass = 0;
}


// This prints the current temp and humidity readings to the display
void printToDisplayTH () {

  // We need to keep track of how big our message is to know how many words to print. Do not change!
  int messageSize = sizeof(currentTempAndHumidity)/sizeof(currentTempAndHumidity[0]);
  
  // "currentPass" is a counter variable that keeps track of which words we have printed so far
  // Once it is equal to "messageSize", we know we have printed all the words and need to start over
  while ( currentPass < messageSize ) {

    // Clear the screen by turning every LED off
    matrix.fillScreen(0);

    // Depending on what we set "boolean oneColor" to, set the text color
    // oneColor is true -- set the text to a single color defined by "redValue", "greenValue", and "blueValue"
    if ( oneColor) matrix.setTextColor(matrix.Color333(redValue, greenValue, blueValue));
    
    // oneColor is false -- set the text to a "hue" that will change as we display the message
    else matrix.setTextColor(matrix.ColorHSV(hue, 255, 255, true));

    // Set the cursor to an X location defined by textX (which will move to the left in order to 'scroll')
    // Y location is set to 1, which corresponds to the second row from the top of the displays
    matrix.setCursor(textX, 1);

    // Print the message!
    matrix.print( currentTempAndHumidity[currentPass] );

   // These two lines change the color hue IF you have set oneColor to false
   // It changes the color by adding "hueIncrease" to the previous color
   hue += hueIncrease;
   if(hue >= 1536) hue -= 1536;

   // Delay function controls how fast or slow the text moves depending on the value you set for "delayTime"
   delay(delayTime);

   // Decrease textX in order to move the message to the left and 'scroll'
   // Increase currentPass because we have finished printing one of the words and need to move to the next word
    if((--textX) < textMin) {
      textX = matrix.width();
      currentPass++;
    }
    
   matrix.swapBuffers(true);
  }

  // Reset our counter to 0 so we know to print the message from the beginning again
  // This happens once currentPass = messageSize and we break out of the "while" loop above
  currentPass = 0;
}
