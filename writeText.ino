/*
 *      PROGRAM: writeText.ino
 *      BY: Samantha Ehrle, Anson Kwan
 *      ON: November 25th, 2019
 *      FOR: The 7th and 8th grade STEM students at South Valley Junior High in Gilbert, AZ.
 *      DOES: Displays scrolling text across two 16x32 AdaFruit displays chained together
 *      
 *  This is a simple program which allows you to write different messages to the displays.
 *  No sensors are utilized within this code besides the displays
 *  
 *  Scroll down and look for "SECTION TO EDIT" comments above each editable portion.
 *  
 *  You can change:
 *    What the message says.
 *    What color the message text is.
 *    Whether or not the text changes colors.
 *    How fast or how slow the message displays.
 * 
 */

// Libraries to provide us with extra functionality
#include <Adafruit_GFX.h>   // Core graphics library
#include <RGBmatrixPanel.h> // Hardware-specific library

// Constants to define which pins on the Arduino Mega we are using.
// Do not change these!
#define CLK 11 
#define LAT 10
#define OE  9
#define A   A0
#define B   A1
#define C   A2
#define n   4 

// A constructor: defines which pins of the Arduino will receive what info needed to control the displays.
// Do not change!
RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, true, 32*n);

// Global variables that we need to be accessible by our entire program.
// Do not change!
const int maxSize     = 10;
int textX       = matrix.width(),
    textMin     = maxSize * -12,
    currentPass = 0,
    hue         = 0,
    passes      = 0;

/**************************************************************************************************
 * 
 *                 SECTION TO EDIT BELOW: YOUR MESSAGE, MESSAGE COLOR, MESSAGE SPEED
 *            
 *     
 *                                  BACKGROUND INFO:
 *     
 *     A "string" is a series of characters surrounded by quotation marks, for example: "This is a string".
 *     In this language, the "string" is an array of characters (chars). An array is like a list.
 *     
 *     This makes sense if you think of a string as a word or a sentence. Just like how a word
 *     or a sentence is a list of characters (made up of letters, spaces, numbers), a string is a list of chars.
 *     
 *     
 ****************************************************************************************************     
 *     
 *     
 *                                  HOW TO MAKE EDITS:
 *     
 *                                     EDIT WORDS:                              
 *                                    
 *     Each string needs to be 10 characters or LESS. 
 *     If it is more than 10 characters, it will get cut off after the 10th one.
 *     You can have as many words as you like.
 *     
 *     Make sure each string is surrounded by quotation marks (like: "string") and has a comma between.
 *            
 *     Easy way: make each word a separate string separated by a comma, for example:
 *          char message[][maxSize] = { "South", "Valley", "Junior", "High", "School" };
 *          
 *     But, you do not need to separate each word IF the total string is LESS than 10 characters:
 *          char message[][maxSize] = { "Saber cats" };
 *          
 *          
 *                                    EDIT COLORS:
 *                                  
 *     You have two choices for colors:                             
 *      
 *      1. The text cycles through different colors.
 *          To do this:
 *              First, set "boolean oneColor = false".
 *              Second, to alter how DRASTICALLY the text changes colors, change the value for "hueIncrease".
 *              
 *              It is set to 35 by default. A higher number will mean a larger change between colors.
 *              A lower number will move through the colors slower, with a smaller change between colors.
 *                    
 *      2. You set the text to one color.
 *          To do this: 
 *              First, set "boolean oneColor = true;" so the program knows you want the text in one color.
 *              Second, to pick the specific color, set "redValue", "greenValue", and "blueValue" to a number between 0 - 7.
 *           
 *              0 means NONE of that color will be mixed in. 7 means A LOT of it will be mixed in.
 *              
 *              For example: redValue = 7, greenValue = 0, blueValue = 0 will display RED.
 *              For example: redValue = 3, greenValue = 0, blueValue = 3 will display PURPLE.
 *                                  
 *                                  
 *                                   EDIT SPEED:
 *                                  
 *     Change the numerical value assigned to "int delayTime".
 *     
 *     Higher numbers = longer delay, slower scroll.
 *     Lower numbers = shorter delay, faster scroll.
 *     It can also be set to 0.
 *     
 **************************************************************************************************/

// "message" is the text to be displayed:
char message[][maxSize] = { "South", "Valley", "Junior", "High", "School" };

// Do you want one color or multi-color? true means one color, false means multi-color
boolean oneColor = false;

// For single color, pick the color by changing these 3 values.
// Set to a number between 0 - 7: 0 means none of that color will be there. 7 means a lot of it will.
int redValue = 3,
    greenValue = 0,
    blueValue = 3;

// For multi-color, pick how MUCH it changes by changing this "hueIncrease" value.
// A higher number changes colors faster/more drastically, a lower number changes colors slower/less drastically.
int hueIncrease = 35;

// "delayTime" changes the speed at which the text scrolls.
// Higher numbers = longer delay, slower scroll.
// Lower numbers = shorter delay, faster scroll.
int delayTime = 10;
    
/******************* END SECTION TO EDIT: MESSAGE, MESSAGE COLOR, MESSAGE SPEED *****************/
/***********************************************************************************************/

// We need to keep track of how big our message is to know how many words to print. Do not change!
int messageSize = sizeof(message)/sizeof(message[0]);

// Set-up function: code that runs only once when the program is first uploaded to the Arduino
void setup() {
  matrix.begin();
  matrix.setTextWrap(false); // Allow text to run off right edge
  matrix.setTextSize(2); // Size 2 is 16 pixels high and will take up the whole display.
}


// Loop function: this code runs over and over again until manually stopped 
void loop() {

  // "currentPass" is a counter variable that keeps track of which words we have printed so far
  // Once it is equal to "messageSize", we know we have printed all the words and need to start over
  while ( currentPass < messageSize ) {

    // Clear the screen by turning every LED off
    matrix.fillScreen(0);

    // Depending on what we set "boolean oneColor" to, set the text color
    // oneColor is true -- set the text to a single color defined by "redValue", "greenValue", and "blueValue"
    if ( oneColor) matrix.setTextColor(matrix.Color333(redValue, greenValue, blueValue));
    
    // oneColor is false -- set the text to a "hue" that will change as we display the message
    else matrix.setTextColor(matrix.ColorHSV(hue, 255, 255, true));

    // Set the cursor to an X location defined by textX (which will move to the left in order to 'scroll')
    // Y location is set to 1, which corresponds to the second row from the top of the displays
    matrix.setCursor(textX, 1);

    // Print the message!
    matrix.print( message[currentPass] );

   // These two lines change the color hue IF you have set oneColor to false
   // It changes the color by adding "hueIncrease" (that you defined earlier) to the previous color
   hue += hueIncrease;
   if(hue >= 1536) hue -= 1536;

   // Delay function controls how fast or slow the text moves depending on the value you set for "delayTime"
   delay(delayTime);

   // Decrease textX in order to move the message to the left and 'scroll'
   // Increase currentPass because we have finished printing one of the words and need to move to the next word
    if((--textX) < textMin) {
      textX = matrix.width();
      currentPass++;
    }
    
   matrix.swapBuffers(true);
  }

  // Reset our counter to 0 so we know to print the message from the beginning again
  // This happens once currentPass = messageSize and we break out of the "while" loop above
  currentPass = 0;
}
