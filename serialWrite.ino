/*
 *      PROGRAM: serialWrite.ino
 *      BY: Samantha Ehrle, Anson Kwan
 *      ON: November 25th, 2019
 *      FOR: The 7th and 8th grade STEM students at South Valley Junior High in Gilbert, AZ.
 *      DOES: Displays text on the screen that is inputted to the Serial Monitor
 *      
 *  Open the serial monitor through the Arduino IDE.
 *  
 *  In the toolbars, click "Tools" and then "Serial monitor"
 *  
 *  When you upload this program, the serial monitor will ask you to input text.
 *  
 *  Input text into the top bar and hit enter. It will appear on the display.
 *  
 *  Input short strings as longer text will get cut off half-way.
 * 
 */

// Libraries to provide us with extra functionality
#include <Adafruit_GFX.h>   // Core graphics library
#include <RGBmatrixPanel.h> // Hardware-specific library

// Constants to define which pins on the Arduino Mega we are using.
// Do not change these!
#define CLK 11  
#define LAT 10
#define OE  9
#define A   A0
#define B   A1
#define C   A2
#define n   2   

// A constructor: defines which pins of the Arduino will receive what info needed to control the displays.
// Do not change!
RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, false, 32*n);

char inChar;                // Temp char storage
char inData1[5 * n + 1];    // First line array (+1 for '\0')
char inData2[5 * n + 1];    // Second line array (+1 for '\0')
uint8_t index = 0;          // Keeps track of what we have printed
bool newPrint;              // Tells us if we need to print something new

// Set-up function: code that runs only once when the program is first uploaded to the Arduino
void setup() {

    // creates our display object and names it 'matrix'
    matrix.begin();
        
    // Set text properties: the size and color
    matrix.setTextSize(1);                                          // size 1 == 8 pixels high
    matrix.setTextColor(matrix.ColorHSV(2048, 255, 255, true));     // Billiant Green

    // Clear the screen by turning every LED off
    matrix.fillScreen(matrix.Color333(0, 0, 0));

    // Begin the serial monitor and print text to the serial monitor
    Serial.begin(9600);
    Serial.write("Welcome!");
    Serial.write('\n');
    Serial.write("Send your text to the RGB Matrix...");
    Serial.write('\n');
}

// Loop function: this code runs over and over again until manually stopped 
void loop() {
    // Check the availability of new input
    while(Serial.available())
    {
        if(index < (5 * n))                 // Store in a string the first 5*n chars (first line)
        {
            inChar = Serial.read();         // Read a character
            inData1[index] = inChar;        // Store it
            index++;                        // Increment where to write next
            inData1[index] = '\0';          // Null terminate the string
            delay(10);
        }
        else                                // Store in a string the second 5*n chars (second line)
        {
            inChar = Serial.read();         // Read a character
            inData2[index - 10] = inChar;   // Store it
            index++;                        // Increment where to write next
            inData2[index - 10] = '\0';     // Null terminate the string
            delay(10);
        }
        newPrint = true;                    // New RGB print enabled
    }
    if(newPrint)
    {
        matrix.fillScreen(matrix.Color333(0, 0, 0));    // Delete the whole panel
        matrix.setCursor(2,0);                          // Set the top left position of the first line
        matrix.print(inData1);                          // First line on 
        matrix.setCursor(2,8);                          // Set the top left position of the second line
        matrix.print(inData2);                          // Second line on
        newPrint = false;                               // New RGB print disabled
        inData1[0] = '\0';                              // String erase
        inData2[0] = '\0';                              // String erase
    }
    index = 0;                              // Restart the storage of characters from 0
}
