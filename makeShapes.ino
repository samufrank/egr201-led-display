/*
 *      PROGRAM: makeShapes.ino
 *      BY: Samantha Ehrle, Anson Kwan
 *      ON: November 25th, 2019
 *      FOR: The 7th and 8th grade STEM students at South Valley Junior High in Gilbert, AZ.
 *      DOES: Draws some various shapes and some text character by character
 *      
 *  In this program, you will edit the function calls instead of the global variables like in the other functions we provided.
 *  
 *  You can change:
 *    Color of shapes
 *    Speed of shapes
 *    Location of shapes
 *    Whether or not text will display
 *    What the text is
 *    Color of the text
 *    
 *    You can also try adding/removing the shapes that we create.
 *    Look at the function calls in void loop() and mimic those!
 * 
 */

// Libraries to provide us with extra functionality
#include <Adafruit_GFX.h>   // Core graphics library
#include <RGBmatrixPanel.h> // Hardware-specific library

// Constants to define which pins on the Arduino Mega we are using.
// Do not change these!
#define CLK 11 
#define LAT 10
#define OE  9
#define A   A0
#define B   A1
#define C   A2
#define n   2  
RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, false, 32*n);

/*
 * 
 * 
 *        In this program, instead of changing variables (like "redValue = 7", you will change the
 *        function calls themselves inside the loop() function!
 *        
 *        This program draws a variety of shapes! You can change the color of those shapes! 
 *        and then writes some text if you set the boolean variable writeText to true: "boolean writeText = true"
 *        
 *        
 *                                HOW TO EDIT COLORS OF SHAPES:
 * 
 *        (7, 7, 7) is white
 *        (0, 0, 0) is black (which is really just leaving the LED off)
 *        (7, 0, 0) is red
 *        (0, 0, 7) is blue
 *        (7, 0, 7) is violet
 *        (7, 7, 0) is yellow
 *        (0, 7, 0) is green
 *        
 *        Try different combinations of numbers from 0-7 for different colors!
 *        
 *                                HOW TO EDIT SPEED OF SHAPES:
 *                                
 *          In the "void loop()" function, after each matrix.drawShape() function call,
 *          there is a second line that says delay(250).
 *          
 *          Change the number inside delay() to whatever you want!
 *          
 *          Remember higher numbers = longer delay = slower scroll.
 *          Lower numbers = shorter delay = quicker scroll.
 *        
 *        
 *        
 *                              HOW TO EDIT X AND Y LOCATIONS OF SHAPES:
 *                              
 *        You can also change where the shapes are drawn by specifying the x and y location.
 *        
 *        X and Y corresponds to the first two parameters in the function call.
 *        (Parameters are the things that get sent to the function called inside the parenthesis).
 *        
 *          For example: matrix.drawPixel(xLocation, yLocation, matrix.Color333(red, green blue);
 *          
 *        The first two parameters are always XLocation, yLocation, for all these functions, not just drawPixel
 *          
 *        You will notice there are already numbers there. Try running this code without changing
 *        anything first to see where stuff shows up, so you have a better idea where to move it.
 *        
 *        The X location corresponds to columns:
 *          x = 0 is the far left of the display
 *          x = 32 is the middle of the display
 *          x = 63 is the far right of the display
 *        
 *        The Y location corresponds to rows:
 *          y = 0 is the very top of the display
 *          y = 8 is the middle row of the display
 *          y = 16 is the very bottom row of the display
 *          
 *          
 *          
 *                          HOW TO EDIT TEXT THAT DISPLAYS / WHETHER THE TEXT DISPLAYS / COLOR OF TEXT:
 *                          
 *         If you want text to show, set "boolean writeText = true". Otherwise, set it to false.                 
 *         
 *         Then, scroll down to the void loop() function to find the lines where text is printed one character at a time.
 *         Like this:  matrix.print('*');
 *         
 *         Edit the characters inside the single quotes ' ' to show what prints.
 *         The characters can be letters, numbers, or special characters.
 *         
 *         
 *         You can edit the colors for each individual letter the same way you do the shapes.
 *         Edit the three numbers in this line:  matrix.setTextColor(matrix.Color333(7,0,0));
 *         
 *         Notice there is one of these lines above each print line.
 *         You can add more characters if you copy and paste any of those two lines (setTextColor and print).
 * 
 */

// This decides if we want to print text to the display as well as shapes.
// True means text will show on the display, false means it will not.
boolean writeText = true;

// Set-up function: code that runs only once when the program is first uploaded to the Arduino
void setup() {  
    matrix.begin();
}

// Loop function: this code runs over and over again until manually stopped 
void loop() {
    // fill the screen with 'black'
    matrix.fillScreen(matrix.Color333(0, 0, 0));
    
    // draw a pixel in solid white
    matrix.drawPixel(16, 0, matrix.Color333(7, 7, 7)); 
    delay(250);
    
    // fix the screen with green
    matrix.fillRect(16, 0, 32, 16, matrix.Color333(0, 7, 0));
    delay(250);
    
    // draw a box in yellow
    matrix.drawRect(16, 0, 32, 16, matrix.Color333(7, 7, 0));
    delay(250);
    
    // draw an 'X' in red
    matrix.drawLine(16, 0, 47, 15, matrix.Color333(7, 0, 0));
    matrix.drawLine(47, 0, 16, 15, matrix.Color333(7, 0, 0));
    delay(250);
    
    // draw a blue circle
    matrix.drawCircle(23, 7, 7, matrix.Color333(0, 0, 7));
    delay(250);
    
    // fill a violet circle
    matrix.fillCircle(39, 7, 7, matrix.Color333(7, 0, 7));
    delay(250);
    
    // fill the screen with 'black'
    matrix.fillScreen(matrix.Color333(0, 0, 0));
    
    // draw a box in Green
    matrix.fillRect(0, 0, 16, 16, matrix.Color333(0, 7, 0));
    delay(250);
    
    // draw a box in white
    matrix.fillRect(16, 0, 32, 16, matrix.Color333(7, 7, 7));
    delay(250);
    
    // draw a box in Red
    matrix.fillRect(48, 0, 16, 16, matrix.Color333(7, 0, 0));
    delay(250);
    
    // fill the screen with 'black'
    matrix.fillScreen(matrix.Color333(0, 0, 0));

    // check if writeText is true. If it is, write text to the display.
    if ( writeText ) {

      // draw some text!
      matrix.setCursor(17, 0);   // start at top left
      matrix.setTextSize(1);    // size 1 == 8 pixels high and takes up half the display. size == 2 is 16 pixels high.
      
      // print letters, single numbers, special characters
      // Remember each print statement can only be one character ('a', '2', '!', etc.)
      // You can change the color of each character with the three numbers at the end
      matrix.setTextColor(matrix.Color333(7,0,0));
      matrix.print('1');
      
      matrix.setTextColor(matrix.Color333(7,4,0)); 
      matrix.print('6');
      
      matrix.setTextColor(matrix.Color333(7,7,0));
      matrix.print('x');
      
      matrix.setTextColor(matrix.Color333(4,7,0)); 
      matrix.print('6');
      
      matrix.setTextColor(matrix.Color333(0,7,0));  
      matrix.print('4');
    
      matrix.setCursor(17, 9);   // next line at x = 17 and y = 9
      matrix.setTextColor(matrix.Color333(0,7,7)); 
      matrix.print('*');
      
      matrix.setTextColor(matrix.Color333(0,4,7)); 
      matrix.print('R');
      
      matrix.setTextColor(matrix.Color333(0,0,7));
      matrix.print('G');
      
      matrix.setTextColor(matrix.Color333(4,0,7)); 
      matrix.print('B');
      
      matrix.setTextColor(matrix.Color333(7,0,4)); 
      matrix.print('*');
    }
}
